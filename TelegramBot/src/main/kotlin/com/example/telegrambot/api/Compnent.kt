package com.example.telegrambot.api

import com.example.telegrambot.Admin
import com.example.telegrambot.AdminRepository
import com.example.telegrambot.Role
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class DataLoader(
    private val adminRepository: AdminRepository,
    @Qualifier("passwordEncoder") private val passwordEncoder: PasswordEncoder,
){


    @PostConstruct
     fun run() {
        if (adminRepository.count()==0L){
            adminRepository.save(
                Admin(
                    "shahboz",
                    passwordEncoder.encode("12345"),
                    Role.ADMIN
                )
            )
        }

    }
}