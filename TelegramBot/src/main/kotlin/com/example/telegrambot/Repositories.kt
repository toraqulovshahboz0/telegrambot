package com.example.telegrambot

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.jpa.repository.support.JpaEntityInformation
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager

@NoRepositoryBean
interface BaseRepository<T : BaseEntity> : JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
    fun trash(id: Long): T
    fun trashList(ids: List<Long>): List<T>
    fun findByIdNotDeleted(id: Long): T?
    fun findAllNotDeleted(pageable: Pageable): Page<T>
    fun findAllNotDeleted(): List<T>
}

class BaseRepositoryImpl<T : BaseEntity>(
    entityInformation: JpaEntityInformation<T, Long>,
    entityManager: EntityManager,
) : SimpleJpaRepository<T, Long>(entityInformation, entityManager), BaseRepository<T> {
    val isNotDeletedSpecification = Specification<T> { root, _, cb -> cb.equal(root.get<Boolean>("deleted"), false) }

    @Transactional
    override fun trash(id: Long) = save(findById(id).get().apply { deleted = true })
    override fun findAllNotDeleted(pageable: Pageable) = findAll(isNotDeletedSpecification, pageable)
    override fun findAllNotDeleted(): List<T> = findAll(isNotDeletedSpecification)
    override fun trashList(ids: List<Long>): List<T> = ids.map { trash(it) }
    override fun findByIdNotDeleted(id: Long): T? =
        findById(id).orElseGet { null }?.run { if (!this.deleted) this else null }
}

interface UserRepository : BaseRepository<User> {
    fun existsByChatId(chatId: Long): Boolean
    fun findByChatId(chatId: Long): User
    fun findByRole(role: Role):Boolean
    fun existsByStatusAndStepAndRole(status: UserStatus, step: Step, role: Role): Boolean
//    fun findByUsernameAndDeletedFalse(username: String): User?


    @Query(
        value = """select * from users u
                   where u.role = ?1 and u.status = 'ONLINE' and u.step = 'WAITING_CHAT' and lang = ?2""",
        nativeQuery = true
    )
    fun getOnlineUser(role: String, lang: String): MutableList<User>

    @Query(
        value = """select * from users u
                   where u.role = 'MODERATOR' and u.status = 'ONLINE' and u.step = 'WAITING_CHAT' and lang = ?1""",
        nativeQuery = true
    )
    fun getOnlineModerators(lang: String): MutableList<User>

    fun findByIdAndDeletedFalse(id: Long): User?
    fun findByChatIdAndDeletedFalse(chatId: Long): User?
    fun findByPhoneAndDeletedFalse(phobe: String): User?

    @Query(
        value = """select * from users u where u.role = ?1""",
        nativeQuery = true
    )
    fun findAllRole(role: String): MutableList<User>

}

interface ChatRepository : BaseRepository<Chat> {
    fun findByModeratorAndChatStatus(moderator: User, status: ChatStatus): Chat
    fun existsByClientAndChatStatus(client: User, status: ChatStatus): Boolean
    fun findFirstByClientAndChatStatusOrderByCreatedDateDesc(client: User, status: ChatStatus): Chat?
    fun findFirstByChatStatusOrderByCreatedDateDesc(status: ChatStatus): Chat?

    @Query(
        value = """select c.moderator_id, c.client_id from chats c left join users u on c.client_id = u.id where u.phone = ?1""",
        nativeQuery = true
    )
    fun findByUserPhone(phone: String, pageable: Pageable): Page<ChatByUserPhoneDto>

    @Query(
        value = """select c.moderator_id, c.client_id from chats c left join users u on c.moderator_id = u.id where u.phone = ?1""",
        nativeQuery = true
    )
    fun findByModeratorPhone(phone: String, pageable: Pageable): Page<ChatByModeratorPhoneDto>
}


interface MarkRepository : BaseRepository<MarkModerators>{
    @Query(
        value = """select  m.moderator_id as moderatorId,AVG(m.mark) as avgMark from mark_moderators m group by m.moderator_id order by AVG(m.mark) desc""",
        nativeQuery = true
    )
    fun getAverageMarkModerators():List<MarkModeratorsDto>
}

interface MessageRepository : BaseRepository<Message> {
    @Query(
        value = """select m.sender_id, m.text_or_caption, m.message_type, m.file_id from messages m left join chats c on m.chat_id = c.id where c.id = ?1""",
        nativeQuery = true
    )
    fun getAllMessageOfChat(idOfChat: Long): List<MessagesOfChat>
}

interface FileItemRepository : BaseRepository<FileItem>
interface AdminRepository : BaseRepository<Admin> {
    fun findByUsername(username: String): Admin?
}



