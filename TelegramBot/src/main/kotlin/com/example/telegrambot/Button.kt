package com.example.telegrambot

import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow

interface Buttons {
    fun shareContact(update: Update): SendMessage
    fun chooseLanguage(update: Update): SendMessage
    fun clientMenu(update: Update): SendMessage
    fun moderatorMenu(update: Update):SendMessage
    fun closeChat(clientChatId: Long, update: Update):SendMessage
}
@Service
class ButtonsImpl() : Buttons {
    override fun shareContact(update: Update): SendMessage {
        val sendMessage = SendMessage()
        sendMessage.text = "Ro`yxatdan o`ting"
        val markup = ReplyKeyboardMarkup()
        markup.selective = true
        markup.resizeKeyboard = true
        val keyboard = ArrayList<KeyboardRow>()
        val keyboardRow = KeyboardRow()
        val button = KeyboardButton()
        button.text = "registration"
        button.requestContact = true
        keyboardRow.add(button)
        keyboard.add(keyboardRow)
        markup.keyboard = keyboard
        sendMessage.replyMarkup = markup
        sendMessage.setChatId(update.message.chatId)
        return sendMessage
    }

    override fun chooseLanguage(update: Update): SendMessage {
        var sendMessage = SendMessage()
        sendMessage.text = "Choose language"
        val markup = ReplyKeyboardMarkup()
        markup.selective = true
        markup.resizeKeyboard = true
        val keyboard = ArrayList<KeyboardRow>()
        val keyboardRow = KeyboardRow()
        val buttonUz = KeyboardButton()
        val buttonRu = KeyboardButton()
        val buttonEn = KeyboardButton()
        buttonUz.text = Lang.UZ.name
        buttonRu.text = Lang.RU.name
        buttonEn.text = Lang.EN.name
        keyboardRow.add(buttonUz)
        keyboardRow.add(buttonRu)
        keyboardRow.add(buttonEn)
        keyboard.add(keyboardRow)
        markup.keyboard = keyboard
        sendMessage.replyMarkup = markup
        sendMessage.setChatId(update.message.chatId)
        return sendMessage

    }

    override fun clientMenu(update: Update): SendMessage {
        val sendMessage = SendMessage()
        sendMessage.text = "Choose menu"
        val markup = ReplyKeyboardMarkup()
        markup.selective = true
        markup.resizeKeyboard = true
        val keyboard = ArrayList<KeyboardRow>()
        val row1 = KeyboardRow()
        val row2 = KeyboardRow()
        val row3 = KeyboardRow()
        val button1 = KeyboardButton()
        val button2 = KeyboardButton()
        val button3 = KeyboardButton()
        button1.text = "start chat"
        button2.text = "change language"
        button3.text = "exit"
        row1.add(button1)
        row2.add(button2)
        row3.add(button3)
        keyboard.add(row1)
        keyboard.add(row2)
        keyboard.add(row3)
        markup.keyboard = keyboard
        sendMessage.replyMarkup = markup
        sendMessage.setChatId(update.message.chatId)
        return sendMessage
    }

    override fun moderatorMenu(update: Update): SendMessage {
        val sendMessage = SendMessage()
        sendMessage.text = "Choose menu"
        val markup = ReplyKeyboardMarkup()
        markup.selective = true
        markup.resizeKeyboard = true
        val keyboard = ArrayList<KeyboardRow>()
        val row1 = KeyboardRow()
        val row2 = KeyboardRow()
        val button1 = KeyboardButton()
        val button2 = KeyboardButton()
        button1.text = "start chat"
        button2.text = "exit"
        row1.add(button1)
        row2.add(button2)
        keyboard.add(row1)
        keyboard.add(row2)
        markup.keyboard = keyboard
        sendMessage.replyMarkup = markup
        sendMessage.setChatId(update.message.chatId)
        return sendMessage

    }

    override fun closeChat(clientChatId: Long, update: Update): SendMessage {
        val sendMessage = SendMessage()
        sendMessage.text = "Thank you for chatting, please rate our moderator"
        val markup = ReplyKeyboardMarkup()
        markup.selective = true
        markup.resizeKeyboard = true
        val keyboard = ArrayList<KeyboardRow>()
        val row1 = KeyboardRow()
        val row2 = KeyboardRow()
        val row3 = KeyboardRow()
        val row4 = KeyboardRow()
        val row5 = KeyboardRow()
        val button1 = KeyboardButton()
        val button2 = KeyboardButton()
        val button3 = KeyboardButton()
        val button4 = KeyboardButton()
        val button5 = KeyboardButton()
        button1.text = "1 mark"
        button2.text = "2 mark"
        button3.text = "3 mark"
        button4.text = "4 mark"
        button5.text = "5 mark"
        row1.add(button1)
        row2.add(button2)
        row2.add(button3)
        row2.add(button4)
        row2.add(button5)
        keyboard.add(row1)
        keyboard.add(row2)
        keyboard.add(row3)
        keyboard.add(row4)
        keyboard.add(row5)
        markup.keyboard = keyboard
        sendMessage.replyMarkup = markup
        sendMessage.setChatId(clientChatId)
        return sendMessage
    }
}