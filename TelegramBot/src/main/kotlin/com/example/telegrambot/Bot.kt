package com.example.telegrambot

import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendDocument
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.methods.send.SendVideo
import org.telegram.telegrambots.meta.api.methods.send.SendVoice
import org.telegram.telegrambots.meta.api.objects.InputFile
import org.telegram.telegrambots.meta.api.objects.Update
import java.io.File
import java.util.*


@Service
class TelegramBot(
    private val userRepository: UserRepository,
    private val userService: UserServiceImpl,
    private val chatService: ChatServiceImpl,
    private val messageRepository: MessageRepository,
    private val fileItemRepository: FileItemRepository,
    private val button: ButtonsImpl,
    private val chatRepository: ChatRepository,
    private val markService: MarkServiceImpl
) : TelegramLongPollingBot() {

    var token: String = "5707529980:AAFBTKuwt2SJoFHNcdEM9XyfsulGOwJtILE"

    var username: String = "https://t.me/TSHJ_bot"

    var lang: Lang? = null

    override fun getBotToken() = token
    override fun getBotUsername() = username
    override fun onUpdateReceived(update: Update) {


        update.run {
            val chatId = update.message.chatId

            var user: User? = null

            userRepository.existsByChatId(chatId).ifFalse {
                userRepository.save(
                    User(
                        chatId, null, null, null, Role.USER, Lang.UZ, Step.REGISTRATION_CHOOSE_LANG, UserStatus.OFFLINE
                    )
                )
                execute(button.chooseLanguage(update))
            }

            //getUser
            userRepository.existsByChatId(chatId).ifTrue {
                user = userRepository.findByChatId(chatId)
            }


            //user panel
            update.hasMessage().ifTrue {

                (user?.role == Role.USER).ifTrue {

                    when {

                        message.hasText() -> {

                            message.text.run {

                                when (this) {

                                    "/start" -> {
                                        (user?.step == Step.START).ifTrue {
                                            changeStatus(user!!, UserStatus.ONLINE)
                                            execute(button.clientMenu(update))
                                            changeStep(user!!, Step.MENU)
                                        }
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            execute(sendTextMessage(chatId, "You are in chat!!!"))
                                        }
                                    }

                                    "change language" -> {
                                        (user?.step == Step.MENU).ifTrue {
                                            execute(button.chooseLanguage(update))
                                            changeStep(user!!, Step.CHOOSE_LANG)
                                        }
                                    }

                                    "UZ", "RU", "EN" -> {
                                        (user?.step == Step.REGISTRATION_CHOOSE_LANG).ifTrue {
                                            user?.lang = Lang.valueOf(this)
                                            changeStep(user!!, Step.SHARE_CONTACT)
                                            execute(button.shareContact(update))
                                            userRepository.save(user!!)
                                        }
                                        (user?.step == Step.CHOOSE_LANG).ifTrue {
                                            user?.lang = Lang.valueOf(this)
                                            user?.step = Step.START
                                            execute(button.clientMenu(update))
                                            userRepository.save(user!!)
                                        }
                                    }

                                    "start chat" -> {
                                        (user?.step == Step.MENU).ifTrue {
                                            changeStep(user!!, Step.WAITING_CHAT)
                                            execute(sendTextMessage(chatId, "You are waiting for chat..."))
                                            userRepository.existsByStatusAndStepAndRole(
                                                UserStatus.ONLINE, Step.WAITING_CHAT, Role.MODERATOR
                                            ).ifTrue {
                                                userService.getOnlineUser(Role.MODERATOR, user!!.lang)[0].run {
                                                    changeStep(user!!, Step.IN_CHAT)
                                                    changeStep(this, Step.IN_CHAT)
                                                    val currentChat =
                                                        chatService.create(this, user!!, ChatStatus.ACTIVE)
                                                    val text = "${user!!.firstName} chatda ..."
                                                    messageRepository.save(
                                                        Message(
                                                            currentChat,
                                                            user!!,
                                                            text,
                                                            MessageType.TEXT,
                                                            update.message.messageId
                                                        )
                                                    )
                                                    execute(sendTextMessage(this.chatId!!, text))
                                                }
                                            }
                                        }
                                    }

                                    "1 mark", "2 mark", "3 mark", "4 mark", "5 mark" -> {
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            execute(button.clientMenu(update))
                                            changeStep(user!!, Step.MENU)
                                            val currentChat =
                                                chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                                    user!!,
                                                    ChatStatus.END
                                                )
                                                    ?: return@ifTrue
                                            markService.create(currentChat, update)
                                            chatRepository.save(currentChat)
                                        }
                                    }

                                    "exit" -> {
                                        (user?.step == Step.MENU).ifTrue {
                                            changeStep(user!!, Step.START)
                                            changeStatus(user!!, UserStatus.OFFLINE)
                                             execute(sendTextMessage(chatId, "Thank you"))
                                        }
                                        (user?.step == Step.WAITING_CHAT).ifTrue {
                                            changeStep(user!!, Step.MENU)
                                            execute(button.clientMenu(update))
                                        }
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            execute(sendTextMessage(chatId, "You are in chat!!!"))
                                        }

                                    }

                                    else -> {
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            val currentChat =
                                                chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                                    user!!,
                                                    ChatStatus.ACTIVE
                                                )
                                                    ?: return@ifTrue
                                            messageRepository.save(
                                                Message(
                                                    currentChat,
                                                    user!!,
                                                    this,
                                                    MessageType.TEXT,
                                                    update.message.messageId
                                                )
                                            )
                                            execute(sendTextMessage(currentChat.moderator?.chatId!!, this))
                                        }
                                        (user?.step == Step.WAITING_CHAT).ifTrue {
                                            val currentChat = if (chatRepository.existsByClientAndChatStatus(
                                                    user!!, ChatStatus.WAITING
                                                )
                                            ) chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                                user!!,
                                                ChatStatus.WAITING
                                            )
                                            else chatRepository.save(Chat(null, user, ChatStatus.WAITING))


                                            currentChat?.let {
                                                messageRepository.save(
                                                    Message(
                                                        it,
                                                        user!!,
                                                        this,
                                                        MessageType.TEXT,
                                                        update.message.messageId
                                                    )
                                                )
                                            }

                                        }
                                    }
                                }
                            }
                        }

                        message.hasContact() -> {
                            (user?.step == Step.SHARE_CONTACT).ifTrue {
                                update.message.contact.run {
                                    user?.firstName = firstName
                                    user?.lastName = lastName
                                    user?.phone = phoneNumber
                                    user?.step = Step.MENU
                                    user?.status = UserStatus.ONLINE
                                    userRepository.save(user!!)
                                    execute(button.clientMenu(update))
                                }
                            }
                        }

                        message.hasVideo() -> {
                            var currentChat: Chat? = null
                            (user?.step == Step.IN_CHAT).ifTrue {
                                currentChat = chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.ACTIVE
                                )
                                    ?: return@ifTrue
                                val video = message.video
                                val caption = message.caption
                                File("./videos").mkdirs()
                                val file = File("./videos/${Date().time}-${video.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, video.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.VIDEO,
                                        update.message.messageId,
                                        video.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        video.mimeType,
                                        video.fileSize
                                    )
                                )
                                execute(sendVideoMessage(currentChat?.moderator?.chatId.toString(), video.fileId, caption))
                            }
                            (user?.step == Step.WAITING_CHAT).ifTrue {
                                currentChat = if (chatRepository.existsByClientAndChatStatus(
                                        user!!, ChatStatus.WAITING
                                    )
                                ) chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.WAITING
                                )
                                else chatRepository.save(Chat(null, user, ChatStatus.WAITING))
                                val video = message.video
                                val caption = message.caption
                                File("./videos").mkdirs()
                                val file = File("./videos/${Date().time}-${video.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, video.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.VIDEO,
                                        update.message.messageId,
                                        video.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        video.mimeType,
                                        video.fileSize
                                    )
                                )
                            }
                        }

                        message.hasVoice() -> {
                            var currentChat: Chat? = null
                            (user?.step == Step.IN_CHAT).ifTrue {
                                currentChat = chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.ACTIVE
                                )
                                    ?: return@ifTrue
                                val voice = message.voice
                                File("./voices").mkdirs()
                                val file = File("./voices/${Date().time}-${voice.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, voice.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        "voice",
                                        MessageType.VOICE,
                                        update.message.messageId,
                                        voice.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        voice.mimeType,
                                        voice.fileSize
                                    )
                                )
                                execute(sendVoiceMessage(currentChat?.moderator?.chatId.toString(), voice.fileId))
                            }
                            (user?.step == Step.WAITING_CHAT).ifTrue {
                                currentChat = if (chatRepository.existsByClientAndChatStatus(
                                        user!!, ChatStatus.WAITING
                                    )
                                ) chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.WAITING
                                )
                                else chatRepository.save(Chat(null, user, ChatStatus.WAITING))
                                val voice = message.voice
                                File("./voices").mkdirs()
                                val file = File("./voices/${Date().time}-${voice.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, voice.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        "voice",
                                        MessageType.VOICE,
                                        update.message.messageId,
                                        voice.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        voice.mimeType,
                                        voice.fileSize
                                    )
                                )
                            }

                        }

                        message.hasDocument() -> {
                            var currentChat: Chat? = null
                            (user?.step == Step.IN_CHAT).ifTrue {
                                currentChat = chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.ACTIVE
                                )
                                    ?: return@ifTrue
                                val document = message.document
                                val caption = message.caption
                                File("./documents").mkdirs()
                                val file = File("./documents/${Date().time}-${document.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, document.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.DOCUMENT,
                                        update.message.messageId,
                                        document.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        document.mimeType,
                                        document.fileSize.toLong()
                                    )
                                )
                                execute(sendDocumendMessage(currentChat?.moderator?.chatId.toString(), document.fileId, caption))
                            }
                            (user?.step == Step.WAITING_CHAT).ifTrue {
                                currentChat = if (chatRepository.existsByClientAndChatStatus(
                                        user!!, ChatStatus.WAITING
                                    )
                                ) chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.WAITING
                                )
                                else chatRepository.save(Chat(null, user, ChatStatus.WAITING))
                                val document = message.document
                                val caption = message.caption
                                File("./documents").mkdirs()
                                val file = File("./documents/${Date().time}-${document.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, document.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.DOCUMENT,
                                        update.message.messageId,
                                        document.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        document.mimeType,
                                        document.fileSize.toLong()
                                    )
                                )
                            }

                        }

                        message.hasPhoto() -> {
                            var currentChat: Chat? = null
                            (user?.step == Step.IN_CHAT).ifTrue {
                                currentChat = chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.ACTIVE
                                )
                                    ?: return@ifTrue
                                val photo = message.photo.last()
                                val caption = message.caption
                                File("./photos").mkdirs()
                                val file = File("./photos/${Date().time}-${photo.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, photo.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.PHOTO,
                                        update.message.messageId,
                                        photo.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        "image/jpeg",
                                        photo.fileSize.toLong()
                                    )
                                )
                                execute(sendPhotoMessage(currentChat?.moderator?.chatId.toString(), photo.fileId, caption))
                            }
                            (user?.step == Step.WAITING_CHAT).ifTrue {
                                currentChat = if (chatRepository.existsByClientAndChatStatus(
                                        user!!, ChatStatus.WAITING
                                    )
                                ) chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                    user!!,
                                    ChatStatus.WAITING
                                )
                                else chatRepository.save(Chat(null, user, ChatStatus.WAITING))
                                val photo = message.photo.last()
                                val caption = message.caption
                                File("./photos").mkdirs()
                                val file = File("./photos/${Date().time}-${photo.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, photo.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat!!,
                                        user!!,
                                        caption,
                                        MessageType.PHOTO,
                                        update.message.messageId,
                                        photo.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        "image/jpeg",
                                        photo.fileSize.toLong()
                                    )
                                )
                            }
                        }

                    }
                }
            }

            //moderator panel
            update.hasMessage().ifTrue {

                (user?.role == Role.MODERATOR).ifTrue {

                    when {

                        message.hasText() -> {

                            message.text.run {

                                when (this) {

                                    "/start" -> {
                                        (user?.step == Step.START).ifTrue {
                                            changeStatus(user!!, UserStatus.ONLINE)
                                            execute(button.moderatorMenu(update))
                                            changeStep(user!!, Step.MENU)
                                        }
                                    }

                                    "start chat" -> {
                                        (user?.step == Step.MENU).ifTrue {
                                            changeStep(user!!, Step.WAITING_CHAT)
                                            userRepository.existsByStatusAndStepAndRole(
                                                UserStatus.ONLINE, Step.WAITING_CHAT, Role.USER
                                            ).ifTrue {
                                                userService.getOnlineUser(Role.USER, user!!.lang)[0].run {
                                                    changeStep(user!!, Step.IN_CHAT)
                                                    changeStep(this, Step.IN_CHAT)
                                                    val currentChat =
                                                        chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                                            this, ChatStatus.WAITING
                                                        )
                                                    if (currentChat != null) {
                                                        currentChat.apply {
                                                            moderator = user
                                                            chatStatus = ChatStatus.ACTIVE
                                                            chatRepository.save(this)
                                                        }
                                                        val messages =
                                                            messageRepository.getAllMessageOfChat(currentChat.id!!)
                                                        messages.forEach {
                                                            (it.message_type == MessageType.TEXT.name).ifTrue {
                                                                execute(
                                                                    sendTextMessage(chatId, it.text_or_caption
                                                                ))
                                                            }
                                                            (it.message_type == MessageType.VOICE.name).ifTrue {
                                                                execute(
                                                                    sendVoiceMessage(chatId.toString(), it.file_id)
                                                                )
                                                            }
                                                            (it.message_type == MessageType.PHOTO.name).ifTrue {
                                                                execute(
                                                                    sendPhotoMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                                )
                                                            }
                                                            (it.message_type == MessageType.VIDEO.name).ifTrue {
                                                                execute(
                                                                    sendVideoMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                                )
                                                            }
                                                            (it.message_type == MessageType.DOCUMENT.name).ifTrue {
                                                                execute(
                                                                    sendDocumendMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                                )
                                                            }
                                                        }

                                                        val text =
                                                            "Assalom alaykum ${this.firstName}, ismin ${user!!.firstName} qanday savollar qiziqtiradi"
                                                        messageRepository.save(
                                                            Message(
                                                                currentChat,
                                                                user!!,
                                                                text,
                                                                MessageType.TEXT,
                                                                update.message.messageId
                                                            )
                                                        )
                                                        execute(sendTextMessage(this.chatId!!, text))
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    "exit" -> {
                                        (user?.step == Step.MENU).ifTrue {
                                            changeStep(user!!, Step.START)
                                            changeStatus(user!!, UserStatus.OFFLINE)
                                            execute(sendTextMessage(chatId, "Rahmat"))
                                        }
                                        (user?.step == Step.WAITING_CHAT).ifTrue {
                                            changeStep(user!!, Step.START)
                                            changeStatus(user!!, UserStatus.OFFLINE)
                                            execute(sendTextMessage(chatId, "Rahmat"))
                                        }
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            val currentChat = chatRepository.findByModeratorAndChatStatus(
                                                user!!, ChatStatus.ACTIVE
                                            )
                                            currentChat.chatStatus = ChatStatus.END
                                            chatRepository.save(currentChat)
                                            changeStep(user!!, Step.WAITING_CHAT)
                                            execute(
                                                sendTextMessage(
                                                    chatId,
                                                    "Chat has closed, you are waiting for chat"
                                                )
                                            )
                                            execute(button.closeChat(currentChat.client?.chatId!!, update))
                                            userRepository.existsByStatusAndStepAndRole(
                                                UserStatus.ONLINE, Step.WAITING_CHAT, Role.USER
                                            ).ifTrue {
                                                userService.getOnlineUser(Role.USER, user!!.lang)[0].run {
                                                    changeStep(user!!, Step.IN_CHAT)
                                                    changeStep(this, Step.IN_CHAT)
                                                    val currentChat1 =
                                                        chatRepository.findFirstByClientAndChatStatusOrderByCreatedDateDesc(
                                                            this, ChatStatus.WAITING
                                                        ) ?: return@ifTrue
                                                    currentChat1.apply {
                                                        moderator = user
                                                        chatStatus = ChatStatus.ACTIVE
                                                        chatRepository.save(this)
                                                    }
                                                    val messages =
                                                        messageRepository.getAllMessageOfChat(currentChat1.id!!)
                                                    messages.forEach {
                                                        (it.message_type == MessageType.TEXT.name).ifTrue {
                                                            execute(
                                                                sendTextMessage(chatId, it.text_or_caption)
                                                            )
                                                        }
                                                        (it.message_type == MessageType.VOICE.name).ifTrue {
                                                            execute(
                                                                sendVoiceMessage(chatId.toString(), it.file_id)
                                                            )
                                                        }
                                                        (it.message_type == MessageType.PHOTO.name).ifTrue {
                                                            execute(
                                                                sendPhotoMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                            )
                                                        }
                                                        (it.message_type == MessageType.VIDEO.name).ifTrue {
                                                            execute(
                                                                sendVideoMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                            )
                                                        }
                                                        (it.message_type == MessageType.DOCUMENT.name).ifTrue {
                                                            execute(
                                                                sendDocumendMessage(chatId.toString(), it.file_id, it.text_or_caption)
                                                            )
                                                        }
                                                    }
                                                    val text =
                                                        "Assalom alaykum ${this.firstName}, qanday savollar qiziqtiradi"
                                                    messageRepository.save(
                                                        Message(
                                                            currentChat1,
                                                            user!!,
                                                            text,
                                                            MessageType.TEXT,
                                                            update.message.messageId
                                                        )
                                                    )
                                                    execute(sendTextMessage(this.chatId!!, text))
                                                }
                                            }
                                        }
                                    }

                                    else -> {
                                        (user?.step == Step.IN_CHAT).ifTrue {
                                            val currentChat =
                                                chatRepository.findByModeratorAndChatStatus(user!!, ChatStatus.ACTIVE)
                                            messageRepository.save(
                                                Message(
                                                    currentChat,
                                                    user!!,
                                                    this,
                                                    MessageType.TEXT,
                                                    update.message.messageId
                                                )
                                            )
                                            execute(sendTextMessage(currentChat.client?.chatId!!, this))
                                        }
                                    }
                                }
                            }
                        }


                        message.hasContact() -> {

                        }

                        message.hasVideo() -> {
                            (user?.step == Step.IN_CHAT).ifTrue {
                                val currentChat =
                                    chatRepository.findByModeratorAndChatStatus(user!!, ChatStatus.ACTIVE)
                                val video = message.video
                                val caption = message.caption
                                File("./videos").mkdirs()
                                val file = File("./videos/${Date().time}-${video.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, video.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat,
                                        user!!,
                                        caption,
                                        MessageType.VIDEO,
                                        update.message.messageId,
                                        video.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        video.mimeType,
                                        video.fileSize
                                    )
                                )
                                execute(sendVideoMessage(currentChat.client?.chatId.toString(), video.fileId, caption))
                            }

                        }

                        message.hasDocument() -> {
                            (user?.step == Step.IN_CHAT).ifTrue {
                                val currentChat =
                                    chatRepository.findByModeratorAndChatStatus(user!!, ChatStatus.ACTIVE)
                                val document = message.document
                                val caption = message.caption
                                File("./documents").mkdirs()
                                val file = File("./documents/${Date().time}-${document.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, document.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat,
                                        user!!,
                                        caption,
                                        MessageType.DOCUMENT,
                                        update.message.messageId,
                                        document.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        document.mimeType,
                                        document.fileSize
                                    )
                                )
                                execute(sendDocumendMessage(currentChat.client?.chatId.toString(), document.fileId, caption))
                            }
                        }

                        message.hasVoice() -> {
                            (user?.step == Step.IN_CHAT).ifTrue {
                                val currentChat =
                                    chatRepository.findByModeratorAndChatStatus(user!!, ChatStatus.ACTIVE)
                                val voice = message.voice
                                File("./voices").mkdirs()
                                val file = File("./voices/${Date().time}-${voice.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, voice.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat,
                                        user!!,
                                        "voice",
                                        MessageType.VOICE,
                                        update.message.messageId,
                                        voice.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        voice.mimeType,
                                        voice.fileSize
                                    )
                                )
                                execute(sendVoiceMessage(currentChat.client?.chatId.toString(), voice.fileId))
                            }

                        }

                        message.hasPhoto() -> {
                            (user?.step == Step.IN_CHAT).ifTrue {
                                val currentChat =
                                    chatRepository.findByModeratorAndChatStatus(user!!, ChatStatus.ACTIVE)
                                val photo = message.photo.last()
                                val caption = message.caption
                                File("./photos").mkdirs()
                                val file = File("./photos/${Date().time}-${photo.fileId}")
                                file.writeBytes(
                                    getFromTelegram(this@TelegramBot, photo.fileId, token)
                                )
                                val message = messageRepository.save(
                                    Message(
                                        currentChat,
                                        user!!,
                                        caption,
                                        MessageType.PHOTO,
                                        update.message.messageId,
                                        photo.fileId
                                    )
                                )
                                fileItemRepository.save(
                                    FileItem(
                                        message,
                                        file.absolutePath,
                                        file.path,
                                        "image/jpeg",
                                        photo.fileSize.toLong()
                                    )
                                )
                                execute(sendPhotoMessage(currentChat.client?.chatId.toString(), photo.fileId, caption))
                            }

                        }


                    }
                }
            }
        }


    }

    fun sendVideoMessage(chatId: String, fileId: String, caption: String?): SendVideo {
        val sendVideo = SendVideo()
        sendVideo.chatId = chatId
        if (!caption.equals(null)) {
            sendVideo.caption = caption
        }
        sendVideo.video = InputFile(fileId)
        return sendVideo
    }

    fun sendDocumendMessage(chatId: String, fileId: String, caption: String?): SendDocument {
        val sendDocument = SendDocument()
        sendDocument.chatId = chatId
        if (!caption.equals(null)) {
            sendDocument.caption = caption
        }
        sendDocument.document = InputFile(fileId)
        return sendDocument
    }

    fun sendPhotoMessage(chatId: String, fileId: String, caption: String?): SendPhoto {
        val sendPhoto = SendPhoto()
        sendPhoto.chatId = chatId
        if (!caption.equals(null)) {
            sendPhoto.caption = caption
        }
        sendPhoto.photo = InputFile(fileId)
        return sendPhoto
    }

    fun sendVoiceMessage(chatId: String, fileId: String): SendVoice {
        val sendVoice = SendVoice()
        sendVoice.chatId = chatId
        sendVoice.voice = InputFile(fileId)
        return sendVoice
    }


    fun sendTextMessage(chatId: Long, text: String): SendMessage {
        val sendMessage = SendMessage()
        sendMessage.setChatId(chatId)
        sendMessage.text = text
        return sendMessage
    }


    fun changeStep(user: User, step: Step) {
        user.step = step
        userRepository.save(user)
    }

    fun changeStatus(user: User, status: UserStatus) {
        user.status = status
        userRepository.save(user)
    }
}