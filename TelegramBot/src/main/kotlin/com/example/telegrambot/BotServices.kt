package com.example.telegrambot

import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.objects.Update

interface UserService {
    fun getOnlineUser(role: Role, lang: Lang): MutableList<User>
    fun getOnlineModerators(lang: Lang): MutableList<User>
}

interface ChatService {
    fun create(moderator: User, user: User, chatStatus: ChatStatus): Chat
}

interface MessageService {
    fun create(chat: Chat, sender: User, update: Update)
}

interface MarkService{
    fun getAllModeratorsAverageMark():List<MarkModeratorsDto>
    fun create(chat: Chat, update: Update)

}

interface FileItemService {
    fun create(message: Message, update: Update)
}

@Service
class UserServiceImpl(
    private val userRepository: UserRepository
) : UserService {
    override fun getOnlineUser(role: Role, lang: Lang) = userRepository.getOnlineUser(role.name, lang.name)
    override fun getOnlineModerators(lang: Lang) = userRepository.getOnlineModerators(lang.name)
}

@Service
class ChatServiceImpl(
    private val repository: ChatRepository,
) : ChatService {
    override fun create(moderator: User, user: User, chatStatus: ChatStatus)  = repository.save(Chat(moderator, user, chatStatus))
}

@Service
class MessageServiceImpl(
    private val repository: MessageRepository,
) : MessageService {
    override fun create(chat: Chat, sender: User, update: Update) {
        repository.save(Message(chat, sender, update.message.text, MessageType.TEXT))
    }
}

@Service
class MarkServiceImpl(
    private val markRepository: MarkRepository
) : MarkService{
    override fun getAllModeratorsAverageMark(): List<MarkModeratorsDto> {
        return  markRepository.getAverageMarkModerators()    }

    override fun create(chat: Chat, update: Update) {
        markRepository.save(MarkModerators(Integer.parseInt(update.message.text.split(" ")[0]), chat.moderator!!, chat.client!!))
    }
}

@Service
class FileItemServiceImpl(
    private val repository: FileItemRepository
) : FileItemService {
    override fun create(message: Message, update: Update) {
        TODO()
    }
}
