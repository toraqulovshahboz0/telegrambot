package com.example.telegrambot


data class BaseMessage(val code: Int, val message: String)

interface ChatByModeratorPhoneDto {
    var modaretor_id: Long?
    var client_id: Long?
}

interface ChatByUserPhoneDto {
    var client_id: Long?
    var modaretor_id: Long?
}

interface MessagesOfChat {
    var sender_id: Long?
    var text_or_caption: String
    var message_type: String
    var file_id: String
}

data class UserForm(
    var chatId: Long? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var phone: String? = null,
    var lang: Lang
) {
    fun toEntity(): User {
        return User(chatId, firstName, lastName, phone, Role.MODERATOR, lang, Step.START, UserStatus.OFFLINE)
    }
}

data class UserDto(
    var id: Long,
    var chatId: Long? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var phone: String? = null,
    var lang: Lang = Lang.UZ,
) {
    companion object {
        fun toDto(user: User): UserDto {
            return UserDto(user.id!!, user.chatId!!, user.firstName!!, user.lastName, user.phone, user.lang)
        }
    }
}

data class ChatDto(
    var moderatorId: Long,
    var clientId: Long,
) {
    companion object {
        fun toDto(chat: Chat): ChatDto {
            return ChatDto(chat.moderator?.id!!, chat.client?.id!!)
        }

    }
}

data class MessageDto(
    var chat: Long? = null,
    val sender: Long? = null,
    var text: String? = null,
    var messageType: MessageType
) {
    companion object {
        fun toDto(message: Message): MessageDto {
            return MessageDto(message.id, message.sender.id, message.textOrCaption, message.messageType)
        }
    }
}

interface MarkModeratorsDto {
    var moderatorId: Long
    var avgMark: Double
}

