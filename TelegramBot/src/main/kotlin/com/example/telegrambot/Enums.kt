package com.example.telegrambot

enum class Role {
    USER, MODERATOR, ADMIN
}

enum class Lang {
    UZ, RU, EN
}

enum class UserStatus {
    ONLINE, OFFLINE
}

enum class ChatStatus {
    ACTIVE, WAITING, END
}

enum class MessageType {
    TEXT, PHOTO, VIDEO, VOICE, DOCUMENT
}

enum class Step {
    START,
    MENU,
    CHOOSE_LANG,
    SHARE_CONTACT,
    REGISTRATION_CHOOSE_LANG,
    WAITING_CHAT,
    IN_CHAT
}

enum class ErrorCode(val code: Int) {
    GENERAL(100),
    OBJECT_NOT_FOUND(101),
    OBJECT_ALLREADY_EXISTS(102),
    OBJECT_IS_NULL(103),
    OBJECT_NOT_ENTERED(104)
}
