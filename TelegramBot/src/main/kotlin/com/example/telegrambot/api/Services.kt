package com.example.telegrambot.api

import com.example.telegrambot.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*

interface UserService {
    fun createModerator(form: UserForm): UserDto
    fun updateModerator(id: Long, form: UserForm): UserDto
    fun getOne(id: Long): UserDto
    fun getOneByChatId(chatId: Long): UserDto
    fun findByPhone(phone: String): UserDto
    fun findAllRole(role: Role): List<UserDto>
    fun delete(id: Long)
}

interface ChatService {
    fun getOne(id: Long): ChatDto
    fun findAll(pageable: Pageable): Page<ChatDto>
    fun getByModeratorPhone(phone: String, pageable: Pageable): Page<ChatByModeratorPhoneDto>
    fun getByUserPhone(phone: String, pageable: Pageable): Page<ChatByUserPhoneDto>
}

interface MessageService {
    fun getMessagesOfChat(idOfChat: Long): List<MessagesOfChat>
}

@Service
class UserServiceImplApi(
    private val repository: UserRepository
) : UserService {
    override fun createModerator(form: UserForm): UserDto {
        if (repository.existsByChatId(form.chatId!!))
            repository.findByChatId(form.chatId!!).apply {
                firstName = form.firstName
                lastName = form.lastName
                chatId = form.chatId
                phone = form.phone
                lang = form.lang
                role = Role.MODERATOR
                status = UserStatus.OFFLINE
                step = Step.START
                return UserDto.toDto(repository.save(this))
            }
        else {
            return UserDto.toDto(repository.save(form.toEntity()))
        }
    }

    override fun updateModerator(id: Long, form: UserForm) = UserDto.toDto(repository.save(getUser(id).apply {
        firstName = form.firstName
        lastName = form.lastName
        chatId = form.chatId
        phone = form.phone
        lang = form.lang
        role = Role.MODERATOR
        status = UserStatus.OFFLINE
        step = Step.START
    }))

    override fun getOne(id: Long) = UserDto.toDto(getUser(id))

    override fun getOneByChatId(chatId: Long) = UserDto.toDto(
        repository.findByChatIdAndDeletedFalse(chatId) ?: throw ObjectNotFoundException(
            chatId.toString(),
            "User"
        )
    )

    override fun findByPhone(phone: String) =
        UserDto.toDto(repository.findByPhoneAndDeletedFalse(phone) ?: throw ObjectNotFoundException(phone, "User"))

    override fun findAllRole(role: Role): List<UserDto> =
        repository.findAllRole(role.toString()).map { UserDto.toDto(it) }

    override fun delete(id: Long) {
        repository.trash(id)
    }

    fun getUser(id: Long) = repository.findByIdNotDeleted(id) ?: throw ObjectNotFoundException(id.toString(), "User")
}

@Service
class ChatServiceImplApi(
    private val repository: ChatRepository
) : ChatService {
    override fun getOne(id: Long) =
        ChatDto.toDto(repository.findByIdNotDeleted(id) ?: throw ObjectNotFoundException(id.toString(), "Chat"))

    override fun findAll(pageable: Pageable) = repository.findAllNotDeleted(pageable).map { ChatDto.toDto(it) }

    override fun getByModeratorPhone(phone: String, pageable: Pageable) =
        repository.findByModeratorPhone(phone, pageable)

    override fun getByUserPhone(phone: String, pageable: Pageable): Page<ChatByUserPhoneDto> =
        repository.findByUserPhone(phone, pageable)

}

@Service
class MessageServiceImplApi(
    private val repository: MessageRepository
) : MessageService {
    override fun getMessagesOfChat(idOfChat: Long) = repository.getAllMessageOfChat(idOfChat)
}

@Service
class AuthService(
    private val adminRepository: AdminRepository
) : UserDetailsService {

    @Throws(NullPointerException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val admin =
            adminRepository.findByUsername(username) ?: throw NullPointerException("Invalid username or password")
        return admin
    }

}

