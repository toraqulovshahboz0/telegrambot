package com.example.telegrambot

fun Boolean.ifTrue(run: () -> Unit) {
        if (this) run()
}

fun Boolean.ifFalse(run: () -> Unit) {
    if (!this) run()
}