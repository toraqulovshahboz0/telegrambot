package com.example.telegrambot.api

import com.example.telegrambot.MarkServiceImpl
import com.example.telegrambot.Role
import com.example.telegrambot.UserForm
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/v1/user")
class UserController(private val service: UserServiceImplApi){

    @PostMapping
    fun create(@RequestBody form: UserForm) = service.createModerator(form)

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long, @RequestBody form: UserForm) = service.updateModerator(id, form)

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: Long) = service.delete(id)

    @GetMapping("{id}")
    fun getOne(@PathVariable id: Long) = service.getOne(id)
    @GetMapping
    fun findAll(@RequestParam role: Role) = service.findAllRole(role)
    @GetMapping("chatid")
    fun getOneByChatId(@RequestParam chatId: Long) = service.getOneByChatId(chatId)
    @GetMapping("phone")
    fun findByPhone(@RequestParam phone: String) = service.findByPhone(phone)
}

@RestController
@RequestMapping("api/v1/chat")
class ChatController(private val service: ChatServiceImplApi){

    @GetMapping("{id}")
    fun getOne(@PathVariable id: Long) = service.getOne(id)

    @GetMapping
    fun findAll(pageable: Pageable) = service.findAll(pageable)

    @GetMapping("moderator")
    fun getByModeratorPhone(@RequestParam phone: String, pageable: Pageable) = service.getByModeratorPhone(phone, pageable)

    @GetMapping("user")
    fun getByUserPhone(@RequestParam phone: String, pageable: Pageable) = service.getByUserPhone(phone, pageable)
}

@RestController
@RequestMapping("api/v1/message")
class MessageController(private val service: MessageServiceImplApi){

    @GetMapping("{id}")
    fun getOne(@PathVariable id: Long) = service.getMessagesOfChat(id)
}

@RestController
@RequestMapping("api/v1/mark")
class MarkController(private val service:MarkServiceImpl){

    @GetMapping
    fun getAllModeratorsAverageMarks()=service.getAllModeratorsAverageMark()
}