package com.example.telegrambot

import org.hibernate.annotations.ColumnDefault
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class BaseEntity(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @CreatedDate @Temporal(TemporalType.TIMESTAMP) var createdDate: Date? = null,
    @LastModifiedDate @Temporal(TemporalType.TIMESTAMP) var modifiedDate: Date? = null,
    @Column(nullable = false) @ColumnDefault(value = "false") var deleted: Boolean = false,
)

@Entity(name = "users")
class User(
    @Column(unique = true) var chatId: Long? = null,
    @Column(length = 16) var firstName: String? = null,
    @Column(length = 16) var lastName: String? = null,
    @Column(length = 16) var phone: String? = null,
    @Enumerated(EnumType.STRING) @Column(length = 64) var role: Role = Role.USER,
    @Enumerated(EnumType.STRING) @Column(length = 4) var lang: Lang = Lang.UZ,
    @Enumerated(EnumType.STRING) var step: Step = Step.MENU,
    @Enumerated(EnumType.STRING) var status: UserStatus = UserStatus.OFFLINE
) : BaseEntity()

@Entity(name = "chats")
class Chat(
    @ManyToOne var moderator: User?,
    @ManyToOne var client: User?,
    @Enumerated(EnumType.STRING) @Column(nullable = false, length = 16) var chatStatus: ChatStatus
) : BaseEntity()

@Entity(name = "messages")
class Message(
    @ManyToOne var chat: Chat,
    @ManyToOne val sender: User,
    var textOrCaption: String? = null,
    @Enumerated(EnumType.STRING) @Column(nullable = false, length = 16) var messageType: MessageType,
    var messageId: Int? = null,
    val fileId: String? = null
) : BaseEntity()

@Entity
class FileItem(
    @ManyToOne var message: Message,
    var url: String? = null,
    val fileName: String? = null,
    val contentType: String? = null,
    val size: Long? = null,
) : BaseEntity()

@Entity
class MarkModerators(
    val mark: Int,
    @ManyToOne val moderator: User,
    @ManyToOne val user: User
) : BaseEntity()

@Entity
class Admin(
    @Column(unique = true, nullable = false) private var username: String,
    @Column private var password: String,
    @Enumerated(EnumType.STRING)  var role: Role=Role.ADMIN
) : BaseEntity(), UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        val grantedAuthorities= mutableListOf<GrantedAuthority>()
        grantedAuthorities.add(SimpleGrantedAuthority(Role.ADMIN.toString()))
        return grantedAuthorities
    }

    override fun getPassword(): String {
        return password
    }

    override fun getUsername(): String {
        return username
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }
}

